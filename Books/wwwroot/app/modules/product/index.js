import angular from 'angular';

// Create the module where our functionality can attach to
let productModule = angular.module('app.product', []);

// Include our UI-Router config settings
import ProductConfig from './product.config';
productModule.config(ProductConfig);


// Controllers
import ProductCtrl from './product.controller';
productModule.controller('ProductCtrl', ProductCtrl);

import AddProductCtrl from './add-product.controller';
productModule.controller('AddProductCtrl', AddProductCtrl);

import AddProductStep1Ctrl from './add-product-step1.controller';
productModule.controller('AddProductStep1Ctrl', AddProductStep1Ctrl);

import AddProductStep2Ctrl from './add-product-step2.controller';
productModule.controller('AddProductStep2Ctrl', AddProductStep2Ctrl);

import AddProductStep3Ctrl from './add-product-step3.controller';
productModule.controller('AddProductStep3Ctrl', AddProductStep3Ctrl);

export default productModule;
