function ProductConfig($stateProvider) {
    'ngInject';

    $stateProvider
    .state('app.product', {
        url: '/product',
        controller: 'ProductCtrl',
        controllerAs: '$ctrl',
        templateUrl: 'modules/product/product.html',
        title: 'Product',
        data: {
            skipLogin: true
        }
    })
    .state('app.addproduct', {
        abstract: true,
        params: {
            productId: null,
        },
        url: '/addproduct',
        controller: 'AddProductCtrl',
        controllerAs: '$ctrl',
        templateUrl: 'modules/product/add-product.html',
        data: {
            skipLogin: true
        }
    })
        .state('app.addproduct.step1', {
            url: '/step1',
            controller: 'AddProductStep1Ctrl',
            controllerAs: '$ctrl',
            templateUrl: 'modules/product/add-product-step1.html',
            title: 'Add Product: Step 1'
        })
        .state('app.addproduct.step2', {
            url: '/step2',
            controller: 'AddProductStep2Ctrl',
            controllerAs: '$ctrl',
            templateUrl: 'modules/product/add-product-step2.html',
            title: 'Add Product: Step 2'
        })
        .state('app.addproduct.step3', {
            url: '/step3',
            controller: 'AddProductStep3Ctrl',
            controllerAs: '$ctrl',
            templateUrl: 'modules/product/add-product-step3.html',
            title: 'Add Product: Step 3'
        });

};

export default ProductConfig;
