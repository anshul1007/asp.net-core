class AddProductCtrl {
    constructor($rootScope, $state) {
        'ngInject';
        this.$state = $state;
        this.$rootScope = $rootScope;
    }

    cancel() {
        this.$state.go('app.product.main', { organizationId : this.$rootScope.organizationId });
    }
}

export default AddProductCtrl;
