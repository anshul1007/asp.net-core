import AddManufacturersCtrl from '../modals/add-manufacturer.controller';

class AddProductStep1Ctrl {
    constructor(RestClient, $uibModal) {
        'ngInject';
        this.RestClient = RestClient;
        this.$uibModal = $uibModal;
        this.step1Validate = false;
        this.product = {
            name: "",
            sku: "",
        }

        this.manufacturers = [ { "id": 1, "name": "Afghanistan" }, { "id": 2, "name": "Akrotiri" }, { "id": 3, "name": "Albania" } ];
    }

    selectManufacturer($item, $model, $label) {
        //alert($item);
    }

    addManufacturers(){
        var modalInstance = this.$uibModal.open({
            //ariaLabelledBy: 'modal-title',
            //ariaDescribedBy: 'modal-body',
            templateUrl: 'app/modules/modals/add-manufacturer.html',
            controller: AddManufacturersCtrl,
            controllerAs: '$ctrl',
            resolve: {
            }
        });

        //var that = this;
        //modalInstance.result.then(function(dataFromModal) {
        //    //if(dataFromModal.result == 'created' || dataFromModal.result == 'updated') {
        //    //    that.$state.transitionTo(that.$state.current, that.$stateParams, { reload: true, inherit: true, notify: true });
        //    //}
        //});
    }

    next(form) {
        this.step1Validate = true;
        if(form.$valid) {
            var data = {
                name: this.product.productName,
                sku: this.product.sku,
            }

            // add organization
            this.RestClient
              .addProduct(data)
              .then(
                (id) => {
                    alert("saved");
                }, (error) => {
                    this.error = error.data;
                });
        }
    }
}

export default AddProductStep1Ctrl;
