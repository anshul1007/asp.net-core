import angular from 'angular';

// Create the module where our functionality can attach to
let notfoundModule = angular.module('app.notfound', []);

// Include our UI-Router config settings
import NotFoundConfig from './notfound.config';
notfoundModule.config(NotFoundConfig);

export default notfoundModule;
