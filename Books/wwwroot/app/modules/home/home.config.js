function HomeConfig($stateProvider) {
    'ngInject';

    $stateProvider
    .state('app.home', {
        url: '/',
        controller: 'HomeCtrl',
        controllerAs: '$ctrl',
        templateUrl: 'modules/home/home.html',
        title: 'Home',
        data: {
            skipLogin: true
        }
    });

};

export default HomeConfig;
