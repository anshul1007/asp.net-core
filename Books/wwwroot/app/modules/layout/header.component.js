
class AppHeaderCtrl {
    constructor($uibModal, $state, $stateParams, User) {
        'ngInject';
        this.$uibModal = $uibModal;
        this.$state = $state;
        this.$stateParams = $stateParams;
        this.User = User;
    }

    createDatatype(){
       
    }

    alterRules(scope) {
        this.$state.go('app.configure-alerts', {scope: scope});
        return false;
    }

    logout() {
        this.User.logout();
    }

    deviceRegistration() {
       
    }

    addOrganization() {
        this.User.addOrganization = {
            organization: null,
            primaryContact: null
        };
        this.$state.go('app.addorganization.step1', {organizationId: null});
        return false;
    }

    addProcessingUnit() {
    }
}

let AppHeader = {
    controller: AppHeaderCtrl,
    controllerAs : '$ctrl',
    templateUrl: 'modules/layout/header.html'
};

export default AppHeader;
