class LoginCtrl {
    constructor($state, User) {
        'ngInject';
        this.User = User;

        this.formData = {
            userName : '',
            password : '',

        }

        this.error = '';

    }

    login() {
        this.User.attemptAuth(this.formData.userName, this.formData.password)
            .then(
                (roles) => {
                    this.error = 'Invalid username or password.'
                });
    }
}

export default LoginCtrl;
