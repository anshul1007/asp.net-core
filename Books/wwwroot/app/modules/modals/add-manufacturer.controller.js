﻿class AddManufacturersCtrl {
    constructor(RestClient, $uibModalInstance) {
        'ngInject';
        this.$uibModalInstance = $uibModalInstance;
        this.RestClient = RestClient;

        //this.model.code = this.$rootScope.organizationCode;
    }
    
    close() {
        this.$uibModalInstance.dismiss();
    }

    save(form) {
        var data = {
            "name" : "test"
        }
        // add organization
        this.RestClient
          .addManufacturer(data)
          .then(
            (id) => {
                alert("saved");
            }, (error) => {
                this.error = error.data;
            });
    }
}

export default AddManufacturersCtrl;