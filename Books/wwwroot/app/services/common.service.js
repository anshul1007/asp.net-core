﻿class Common  {

    constructor($http) {
        'ngInject';
        
        this._$http = $http;
    }

    format(string) {
        var args = Array.prototype.slice.call(arguments, 1);
        return string.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined' ? args[number] : match;
        });
    }

    guid() {
        var d = new Date().getTime();
        if(window.performance && typeof window.performance.now === "function"){
            d += performance.now(); //use high-precision timer if available
        }
        var uuid = 'xxxxxxxx-xxxx-xxxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = (d + Math.random()*16)%16 | 0;
            d = Math.floor(d/16);
            return (c=='x' ? r : (r&0x3|0x8)).toString(16);
        });
        return uuid;
    }

    getDates (days) {
        if (days > 0) {
            var todayTimeStamp = +new Date; // Unix timestamp in milliseconds
            var xDayTimeStamp = 1000 * 60 * 60 * 24 * days; // Milliseconds in a day
            var diff = todayTimeStamp - xDayTimeStamp;
            var fromDate = new Date(diff);
            var toDate = new Date(todayTimeStamp);
            return {
                "toDate": toDate,
                "fromDate": fromDate,
                "interval": 900,
                "days" : days
            };
        } else if(days === -1) {
            var todayTimeStamp = +new Date; // Unix timestamp in milliseconds
            var fromDate = new Date(new Date().getFullYear(), 0, 1);
            var toDate = new Date(todayTimeStamp);
            return {
                "toDate": toDate,
                "fromDate": fromDate,
                "interval": 900,
                "days" : days
            };
        }
    };

    getData(url) {
        return this._$http({
            url: url,
            method: 'GET',
        }).then((res) => res.data);
    }

    postData(url, data) {
        return this._$http({
            url: url,
            method: 'POST',
            data : data,
            headers : {
                'Content-Type': 'application/json'
            }
        }).then((res) => res.data);
    }

    putData(url, data) {
        return this._$http({
            url: url,
            method: 'PUT',
            data : data,
            headers : {
                'Content-Type': 'application/json'
            }
        }).then((res) => res.data);
    }

    deleteData(url, data) {
        return this._$http({
            url: url,
            method: 'DELETE',
            data : data,
            headers : {
                'Content-Type': 'application/json'
            }
        }).then((res) => res.data);
    }
}

export default Common;