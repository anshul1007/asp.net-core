class RestClient {
    constructor(AppConstants, Common) {
        'ngInject';

        this._AppConstants = AppConstants;
        this._Common  = Common;
    }

    addProduct(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.addProduct);
        return this._Common.postData(url, data);
    }

    addManufacturer(data) {
        var url = this._Common.format(this._AppConstants.api + this._AppConstants.routes.addManufacturer);
        return this._Common.postData(url, data);
    }
}

export default RestClient;