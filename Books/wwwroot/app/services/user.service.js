export default class User {
    constructor(AppConstants, RestClient, $state, $q, $window) {
        'ngInject';

        this.RestClient = RestClient;
        this.$state = $state;
        this.$q = $q;
        this.$window = $window;
        this.current = null;
        this.addOrganization = null;
        //this.checkCurrentUser().then(
        //    () => {
        //        if(this.$state.current.name === 'app.login') {
        //            this.$state.go('app.home');
        //        }
        //    }, 
        //    () => {
        //        if(this.$state.current.name !== 'app.login') {
        //            this.$state.go('app.login');
        //        }
        //    });
    }

    checkCurrentUser() {
        var deferred = this.$q.defer();
        this.RestClient
            .getCurrentUser()
            .then(
                (res) => {
                    this.current = res;
                    deferred.resolve('');
                }, (error) => {
                    this.current = null;
                    deferred.reject(error);
                });
        return deferred.promise;
    }

    attemptAuth(userName, password) {
        var deferred = this.$q.defer();
        this.RestClient
            .login(userName, password)
            .then(
                (res) => {
                    this.current = res;
                    deferred.resolve('');
                    this.$state.go("app.home");
                }, (error) => {
                    this.current = null;
                    deferred.resolve(error);
                });
        return deferred.promise;
    }

    logout() {
        this.RestClient
                .logout()
                .then(
                    () => {
                        this.current = null;
                        this.$window.location.reload(true)
                    });
    }

}
