function authInterceptor(AppConstants, $q, $injector) {
    'ngInject'
    var modalInstance = null;
    return {
        // automatically attach Authorization header
        request: function(config) {
            return config;
        },

        // Handle 401
        responseError: function(rejection) {
            if (rejection.status === 401) {
                var stateService = $injector.get('$state');
                stateService.go('app.login');
                //$window.location.href = '/#/login';
            } else if(rejection.status === 403) {
                var uibModalService = $injector.get('$uibModal');
                if(!modalInstance) {
                    modalInstance = uibModalService.open({
                        templateUrl: 'modules/modals/no-permission.html',
                        controller: ['$uibModalInstance', function($uibModalInstance) {
                            this.$uibModalInstance = $uibModalInstance;
                            this.close = function() {
                                this.$uibModalInstance.dismiss();
                            }
                        }],
                        controllerAs: '$ctrl'
                    });

                    modalInstance.result.then(angular.noop, function (e) {
                        modalInstance = null;
                    });
                }
            }
            return $q.reject(rejection);
        }

    }
}

export default authInterceptor;
