import BaseApi from '../config/app.baseApi';

const AppConstants = {
    api:  BaseApi.origin + '/api/',
    appName: 'Books',
    routes:{
        'addProduct' : 'products',
        'addManufacturer' :'manufacturer'
    },
    paginationOptions: {
        maxSize: 10,
        itemsPerPage: 25,
        totalItems: 0,
        currentPage: 1
    }
};

export default AppConstants;
