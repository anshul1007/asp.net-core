function AppRun($rootScope, $state, User) {
    'ngInject';

    $rootScope.$on('$stateChangeStart', (event, toState, toParams) => {
        var skipLogin = !!toState.data && !!toState.data.skipLogin;
        if (skipLogin !== true && !User.current) {
            event.preventDefault();
            User.checkCurrentUser().then(
            () => {
                $state.go(toState, toParams);
            }, 
            () => {
                $state.go('app.login');
            });
        }
        else if (skipLogin !== true) {
            User.checkCurrentUser();
        }
    });
}

export default AppRun;
