import authInterceptor from './auth.interceptor'

function AppConfig($httpProvider, $stateProvider, $locationProvider, $urlRouterProvider) {
  'ngInject';

  $httpProvider.interceptors.push(authInterceptor);

  $httpProvider.defaults.withCredentials = true;

  /*
    If you don't want hashbang routing, uncomment this line.
    Our tutorial will be using hashbang routing though :)
  */
  // $locationProvider.html5Mode(true);

  $locationProvider.html5Mode(false);
  $locationProvider.hashPrefix('');

  $stateProvider
  .state('app', {
    abstract: true,
    templateUrl: 'modules/layout/app-view.html',
    resolve: {
      //auth: function(User) {
      //  return User.verifyAuth();
      //}
    }
  });

  $urlRouterProvider.otherwise('/');

}

export default AppConfig;
