﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Books.Services
{
    public class DebugMailServices : IMailServices
    {
        public void SendMail(string to, string from, string subject)
        {
            Console.WriteLine($"sending mail to : {to} from : {from}");
        }
    }
}
