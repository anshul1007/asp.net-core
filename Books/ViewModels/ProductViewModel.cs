﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Books.ViewModels
{
    public class ProductViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Sku { get; set; }
        public int Quantity { get; set; }
        public int StockStatus { get; set; }
        public string Image { get; set; }
        public double Price { get; set; }
        public int Viewd { get; set; }
        public double MinimumSellingPrice { get; set; }
        public double Cost { get; set; }
    }
}
