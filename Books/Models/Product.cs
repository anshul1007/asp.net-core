﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Books.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Sku { get; set; }
        public int Quantity { get; set; }
        public int StockStatus { get; set; }
        public string Image { get; set; }
        public Manufacturer Manufacture { get; set; }
        public double Price { get; set; }
        public int Viewd { get; set; }
        public double MinimumSellingPrice { get; set; }
        public double Cost { get; set; }
    }

    public class Manufacturer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
    }

    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public string Image { get; set; }
        public Category ParentId { get; set; }
        public int SortOrder { get; set; }
        public int Status { get; set; }
    }

    public class ProductCategory
    {
        public int ProductId { get; set; }
        public ICollection<Category> Categories { get; set; }
    }

    public class ProductTag
    {
        public int Id { get; set; }
        public Product Product { get; set; }
        public string Tag { get; set; }
    }

    public class Cart
    {
        public Guid Id { get; set; }
        public BookUser Customer { get; set; }
        public bool IsGuest { get; set; }
    }

    public class CartProduct
    {
        public Cart Cart { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}
