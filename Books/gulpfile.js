﻿/// <binding />
/*
This file in the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/

var gulp = require('gulp');
var notify = require('gulp-notify');
var rename = require('gulp-rename');
var source = require('vinyl-source-stream');
var browserify = require('browserify');
var babelify = require('babelify');
var ngAnnotate = require('browserify-ngannotate');
var templateCache = require('gulp-angular-templatecache');

var interceptErrors = function (error) {
    var args = Array.prototype.slice.call(arguments);

    // Send error to notification center with gulp-notify
    notify.onError({
        title: 'Compile Error',
        message: '<%= error.message %>'
    }).apply(this, args);

    // Keep gulp from hanging on this task
    this.emit('end');
};

var viewFiles = "./wwwroot/app/**/*.html";
// Where our files are located
var jsFiles = "./wwwroot/app/**/*.js";

var cssLibraries = ['./css/style.css',
    './node_modules/bootstrap/dist/css/bootstrap.min.css'];

var jsLibraries = ['./node_modules/bootstrap/dist/js/bootstrap.min.js',
'./node_modules/jquery/dist/jquery.min.js'];


gulp.task('default', ['browserify', 'copyFiles'], function () {
    // place code for your default task here
    gulp.watch(viewFiles, ['views']);
    gulp.watch(jsFiles, ['browserify']);
});

gulp.task('copyFiles', function () {
    gulp.src(cssLibraries)
    .pipe(gulp.dest('./wwwroot/css'));
    gulp.src(jsLibraries)
    .pipe(gulp.dest('./wwwroot/js'));
    gulp.src('./node_modules/bootstrap/dist/fonts/*.*')
   .pipe(gulp.dest('./wwwroot/fonts'));
});

gulp.task('views', function () {
    return gulp.src(viewFiles)
        .pipe(templateCache({
            standalone: true
        }))
        .on('error', interceptErrors)
        .pipe(rename("app.templates.js"))
        .pipe(gulp.dest('./wwwroot/app/config/'));
});

gulp.task('browserify', ['views'], function () {
    return browserify('./wwwroot/app/app.js')
        .transform(babelify, { presets: ["es2015"] })
        .transform(ngAnnotate)
        .bundle()
        .on('error', interceptErrors)
        //Pass desired output filename to vinyl-source-stream
        .pipe(source('main.js'))
        // Start piping stream to tasks!
        .pipe(gulp.dest('./wwwroot/js/'));
});