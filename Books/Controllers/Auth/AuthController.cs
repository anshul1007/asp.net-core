﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Books.Models;
using Books.ViewModels;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Books.Controllers.Auth
{
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        private SignInManager<BookUser> _signInManager;

        public AuthController(SignInManager<BookUser> signInManager)
        {
            _signInManager = signInManager;
        }

        // POST api/values
        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody]LoginViewModel loginViewModel)
        {
            if (ModelState.IsValid)
            {
                var signInResult = await _signInManager.PasswordSignInAsync(loginViewModel.UserName, loginViewModel.Password, true, false);

                if (signInResult.Succeeded)
                {
                    return Ok("Done!!");
                }
                else
                {
                    ModelState.AddModelError("", "Invalid user name or password");
                }

            }
            return BadRequest(ModelState);
        }

        [HttpDelete]
        [Route("logout")]
        public async Task<IActionResult> Logout()
        {

            if (this.User.Identity.IsAuthenticated)
            {
                await _signInManager.SignOutAsync();
            }
            return Ok("Done!!");
        }
    }
}
