﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Books.DAO;
using Microsoft.AspNetCore.Authorization;
using Books.ViewModels;
using Books.Models;
using AutoMapper;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Books.Controllers.Api
{
    [Route("api/products")]
    public class ProductController : Controller
    {
        private IBookRepository _repository;

        public ProductController(IBookRepository repository)
        {
            _repository = repository;
        }

        // GET: api/values
        //[HttpGet]
        ////[Authorize]
        //public IActionResult Get()
        //{
        //    return Ok(_repository.GetAllTrips());
        //}

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ProductViewModel product)
        {
            if (ModelState.IsValid)
            {
                var newProduct = Mapper.Map<Product>(product);
                _repository.AddProduct(newProduct);
                if (await _repository.SaveChangesAsync())
                {
                    return Ok("Saved!!");
                }
            }
            return BadRequest(ModelState);
        }

        [HttpGet]
        [Route("~/api/manufacturer")]
        public string Get()
        {
            return "Testes!!";
        }

        [HttpPost]
        [Route("~/api/manufacturer")]
        public async Task<IActionResult> Post([FromBody] Manufacturer manufacturer)
        {
            _repository.AddManufacturer(manufacturer);
            if (await _repository.SaveChangesAsync())
            {
                return Ok("Saved!!");
            }
            return BadRequest(ModelState);
        }
    }
}
