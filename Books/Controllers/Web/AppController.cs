﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Books.Models;
using Books.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Books.DAO;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Books.Controllers.Web
{
    public class AppController : Controller
    {
        private IConfigurationRoot _config;
        private ILogger<AppController> _logger;
        private IMailServices _mailservice;
        private IBookRepository _repository;

        public AppController(IBookRepository repository, 
            IMailServices mailservice, 
            IConfigurationRoot config, 
            ILogger<AppController> logger)
        {
            _mailservice = mailservice;
            _repository = repository;
            _config = config;
            _logger = logger;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
            //try
            //{
            //    //_mailservice.SendMail(_config["MailSettings:To"], "Test2", "test3");
            //    //var data = _repository.GetAllTrips();
            //    return View();
            //}
            //catch (Exception ex)
            //{
            //    _logger.LogError("Failed");
            //    return Redirect("/about");
            //    throw;
            //}
        }
    }
}
