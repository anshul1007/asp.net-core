﻿using Books.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Books.DAO
{
    public class BookContextSeedData
    {
        private BookContext _context;
        private UserManager<BookUser> _userManager;

        public BookContextSeedData(BookContext context, UserManager<BookUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task EnsureData()
        {
            if (await _userManager.FindByEmailAsync("anshul.1007@gmail.com") == null)
            {
                var user = new BookUser()
                {
                    UserName = "anshul",
                    Email = "anshul.1007@gmail.com"
                };

                await _userManager.CreateAsync(user, "Anshul@123");
            }

            //if (!_context.Trips.Any())
            //{
            //    var usTrip = new Trip()
            //    {
            //        DateCreated = DateTime.UtcNow,
            //        Name = "US Trips",
            //        Stops = new List<Stop>()
            //        {
            //            new Stop() { Order = 0, Latitude =  33.748995, Longitude =  -84.387982, Name = "Atlanta, Georgia", Arrival = DateTime.Parse("Jun 3, 2014") },
            //            new Stop() { Order = 1, Latitude =  48.856614, Longitude =  2.352222, Name = "Paris, France", Arrival = DateTime.Parse("Jun 4, 2014") },
            //            new Stop() { Order = 2, Latitude =  50.850000, Longitude =  4.350000, Name = "Brussels, Belgium", Arrival = DateTime.Parse("Jun 25, 2014") },
            //            new Stop() { Order = 3, Latitude =  51.209348, Longitude =  3.224700, Name = "Bruges, Belgium", Arrival = DateTime.Parse("Jun 28, 2014") },
            //            new Stop() { Order = 4, Latitude =  48.856614, Longitude =  2.352222, Name = "Paris, France", Arrival = DateTime.Parse("Jun 30, 2014") },
            //            new Stop() { Order = 5, Latitude =  51.508515, Longitude =  -0.125487, Name = "London, UK", Arrival = DateTime.Parse("Jul 8, 2014") },
            //        }
            //    };

            //    _context.Trips.Add(usTrip);
            //    _context.Stops.AddRange(usTrip.Stops);

            //    await _context.SaveChangesAsync();
            //}
        }
    }
}
