﻿using Books.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Books.DAO
{
    public interface IBookRepository
    {
        //IEnumerable<Product> GetAllProducts();

        void AddProduct(Product product);

        void AddManufacturer(Manufacturer manufacturer);

        Task<bool> SaveChangesAsync();
    }
}