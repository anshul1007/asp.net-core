﻿using Books.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Books.DAO
{
    public class BookRepository : IBookRepository
    {
        private BookContext _context;

        public BookRepository(BookContext context)
        {
            _context = context;
        }

        public void AddManufacturer(Manufacturer manufacturer)
        {
            _context.Add(manufacturer);
        }

        public void AddProduct(Product product)
        {
            _context.Add(product);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return (await _context.SaveChangesAsync() > 0);
        }
    }
}
