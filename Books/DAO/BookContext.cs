﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Books.Models;

namespace Books.DAO
{
    public class BookContext : IdentityDbContext<BookUser>
    {
        private IConfigurationRoot _config;

        public BookContext(IConfigurationRoot config, DbContextOptions options)
            : base(options)
        {
            _config = config;
        }
        public DbSet<Product> Products { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.UseSqlServer(connectionString: _config["ConnectionStrings:BookContextConnection"]);
        }
    }
}
